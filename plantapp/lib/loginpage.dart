import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StartPage extends StatefulWidget {
  const StartPage({super.key});

  @override
  State createState() => _MainAppState();
}

class _MainAppState extends State<StartPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/plant.png'),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Enjoy your ',
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 34.20,
                ),
              ),
              const SizedBox(
                width: 65,
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'life with ',
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w400,
                  fontSize: 34.20,
                ),
              ),
              Text(
                'Plants',
                style: GoogleFonts.poppins(
                  fontWeight: FontWeight.w600,
                  fontSize: 34.20,
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18.0),
            child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return const Login();
                    }),
                  );
                },
                style: const ButtonStyle(
                    fixedSize: MaterialStatePropertyAll(
                      Size(320, 50),
                    ),
                    backgroundColor: MaterialStatePropertyAll(
                        Color.fromRGBO(62, 102, 24, 1))),
                child: const Text(
                  'Get Started   >',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 23,
                      fontWeight: FontWeight.w500),
                )),
          )
        ],
      ),
    );
  }
}

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 128,
            width: 128,
            decoration: const BoxDecoration(
              borderRadius: 
            ),
          ),
        ],
      ),
    );
  }
}
