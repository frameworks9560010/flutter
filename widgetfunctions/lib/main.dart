import 'package:flutter/material.dart';
import 'package:widgetfunctions/DataBase/farmerbill.dart';
import 'package:widgetfunctions/DataBase/gateEntry.dart';
import 'routs/APMCapp.dart';
import 'package:widgetfunctions/DataBase/biddingdata.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  gateEntryDatabaseFunction();
  farmerBillDatabaseFunction();
  biddingDatabaseFunction();
  runApp(const APMCApp());
}
