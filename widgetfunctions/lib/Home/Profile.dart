import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Bhagyashree Patil"),
      ),
      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.width / 2 + 145,
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.all(0),
                  child: Container(
                    height: 270,
                    width: double.infinity,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/profile/sh.jpg'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                      ),
                    ),
                  ),
                ),
                const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: CircleAvatar(
                        backgroundImage:
                            AssetImage('assets/profile/shradha.jpeg'),
                        radius: 80.0,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 7,
          ),
          const Text(
            "Bhagyashree Patil",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
          ),
          const Text("(Software Devloper)"),
          const SizedBox(
            height: 50,
          ),
          const Text(
            "Details ",
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
          ),
          const Text(
            "contact : 9309577322",
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
          ),
          const Text(
            "contact : bhagyashreepatil@Gmail.com",
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
          ),
          const Text(
            "contact : BhagyaInfoTech",
            style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
          )
        ],
      ),
    );
  }
}
