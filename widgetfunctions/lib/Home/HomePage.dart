import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
//appBar
      appBar: AppBar(
        shadowColor: Colors.white,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: const Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 18,
            ),
            Row(
              children: [
                CircleAvatar(
                  radius: 38,
                  backgroundImage: AssetImage('assets/logo.jpeg'),
                ),
                SizedBox(width: 5),
                Center(
                  child: Text(
                    'APMC SYSTEM',
                    style: TextStyle(
                      color: Color.fromARGB(255, 10, 0, 0),
                      fontSize: 33,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),

      //impltation of Body
      body: Column(
        children: [
          const SizedBox(
            height: 30,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                padding: const EdgeInsets.all(25),
                decoration: BoxDecoration(
                  color: const Color.fromARGB(255, 226, 226, 231),
                  borderRadius: BorderRadius.circular(30),
                ),
                child: Row(
                  children: [
                    const SizedBox(width: 15),

                    //live market button
                    ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/liveMarket');
                        },
                        clipBehavior: Clip.antiAlias,
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          padding: EdgeInsets.zero,
                        ),
                        child:
                            Image.asset('assets/liveMarket.jpeg', height: 172)),
                    const SizedBox(width: 49),

                    //Treder Button
                    ElevatedButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/trader');
                      },
                      clipBehavior: Clip.antiAlias,
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        padding: EdgeInsets.zero,
                      ),
                      child: Image.asset(
                        'assets/traders.jpeg',
                        height: 172,
                      ),
                    ),
                    const SizedBox(width: 49),
                    ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/farmer');
                        },
                        clipBehavior: Clip.antiAlias,
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          padding: EdgeInsets.zero,
                        ),
                        child: Image.asset('assets/farmer.jpeg', height: 172)),
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(height: 30),

          //wether button
          Container(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: const Color.fromARGB(255, 226, 226, 231),
                borderRadius: BorderRadius.circular(40)),
            child: Row(
              children: [
                const SizedBox(width: 0),
                ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/weather');
                  },
                  clipBehavior: Clip.antiAlias,
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40.0),
                    ),
                    padding: const EdgeInsets.all(0),
                  ),
                  child: Image.asset('assets/wether.jpeg', height: 118),
                ),
                const SizedBox(
                  width: 0,
                )
              ],
            ),
          ),
          const SizedBox(height: 30),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: const Color.fromARGB(255, 226, 226, 231),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(28.0),
                  child: Row(
                    children: [
                      const SizedBox(width: 10),

                      //letest News button
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/latestNews');
                          },
                          clipBehavior: Clip.antiAlias,
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset('assets/news.jpeg', height: 158)),
                      const SizedBox(width: 29),

                      //farmer button
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/aboutAPMC');
                          },
                          clipBehavior: Clip.antiAlias,
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset('assets/aboutApmc.jpeg',
                              height: 158)),

                      const SizedBox(width: 40),

                      //contactUs button
                      ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/contactUs');
                          },
                          clipBehavior: Clip.antiAlias,
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset('assets/contactUs.jpeg',
                              height: 158)),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),

      bottomNavigationBar: Container(
        width: double.infinity,
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: Colors.white,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, '/profile');
              },
              icon: const Icon(
                Icons.account_circle,
                size: 40,
                color: Colors.yellow,
              ),
            ),
            const SizedBox(
              width: 55,
            ),
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, '/liveMarket');
              },
              icon: const Icon(
                Icons.home,
                color: Colors.blue,
                size: 50,
              ),
            ),
            const SizedBox(
              width: 55,
            ),
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, '/latestNews');
              },
              icon: const Icon(
                Icons.notifications,
                size: 40,
                color: Colors.yellow,
              ),
            )
          ],
        ),
      ),
    );
  }
}
