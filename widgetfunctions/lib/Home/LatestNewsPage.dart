import 'package:flutter/material.dart';

class LatestNewsPage extends StatelessWidget {
  final List<Map<String, String>> newsList = [
    {
      'title': 'West Indies win a Test at the Gabba after 35 years',
      'description':
          'west indies defeated australia by eight runs at the gabba in the second test to level the twomatch series 11 it is west indies first test win at the gabba in more than 35 years and on australian soil after 27 years west indies fast bowler shamar joseph took seven wickets for 68 runs in australias second innings',
      'Image': 'assets/News/westindis.jpg'
    },
    {
      'title':
          'Explicit fake images of Taylor Swift prove laws haven kept pace with tech, experts say',
      'description':
          'Explicit AI-generated photos of one of the worlds most famous artists spread rapidly across social media this week, highlighting once again what experts describe as an urgent need to crack down on technology and platforms that make it possible for harmful images to be shared. Fake photos of Taylor Swift that depicted the singer-songwriter in sexually suggestive positions were viewed tens of millions of times on X, previously known as Twitter, before being removed. One photo, shared by a single user, was seen more than 45 million times before the account was suspended. But by then, the widely-shared photo had been immortalized elsewhere on the internet.',
      'Image': 'assets/News/taylor.jpg'
    },
    {
      'title':
          'Supreme Court Turns 75, Chief Justice Explains What "Near Future" Holds',
      'description':
          'Chief Justice of India DY Chandrachud today said the Supreme Court will soon migrate its digital data to a cloud-based infrastructure, while addressing a gathering of judges and other legal luminaries as the court turned 75 years today.',
      'Image': 'assets/News/modi.webp'
    },
    {
      'title':
          'Nitish Kumar Quit Because Of A "Congress Conspiracy": JDUs Big Claim',
      'description':
          'After days of uncertainty, Nitish Kumar broke ties with the Opposition INDIA alliance and has again joined hands with the Bharatiya Janata Party (BJP), marking his fifth flip-flop in a decade.',
      'Image': 'assets/News/md.webp'
    },
    {
      'title':
          'Rashmika Mandanna On Working With Chhava Co-Star Vicky Kaushal: "You Are Such A Gem"',
      'description':
          'Actress Rashmika Mandanna recently wrapped the shoot for her upcoming film Chhava with Vicky Kaushal. On Saturday, the Animal actor individually wrote small notes for the director, producer, and other team of the movie. For the Sam Bahadur star with whom she is working for the first time, she wrote, "Its been such a pleasure working with you. You are just tooooo warm and kind (except for the last day where you were just taking my case) but most days you were amazing. I am kidding..you are such a gem. I will always wish the best for you man. Was such a pleasure. mom has told me to convey regards to you.” Talking about the films director Laxman Utekar, she wrote, “I just wonder how can a man handle such a bigg set with at least 1500 working people with such calm and poise..sir you have seen me as Yesubai when no one in the world couldve even thought about this and I truly wonder how...and not just me...the whole country will wonder how.”',
      'Image': 'assets/News/rash.webp'
    },
    {
      'title':
          'Shashi Tharoors Word Of The Day Swipe At Nitish Kumar Amid Bihar Drama',
      'description':
          'Senior Congress leader Shashi Tharoor on Sunday took a swipe at JD(U) leader Nitish Kumar over his latest volte-face after he walked out of the Mahagathbandhan government in Bihar, dubbing him as a "snollygoster" or a shrewd and unprincipled politicion',
      'Image': 'assets/News/shashi.webp'
    },
    {
      'title':
          'Shirley Setia reveals she was discouraged from learning music; says her family saw it as a hobby and not as a profession',
      'description':
          'Shirley Setia, who is slowly but steadily making her .Read more at:http://timesofindia.indiatimes.com/articleshow/96755925.cms?utm_source=contentofinterest&utm_medium=text&utm_campaign=cppst',
      'Image': 'assets/News/setia.jpg'
    }
  ];



  LatestNewsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Latest News'),
      ),
      body: ListView.builder(
        itemCount: newsList.length,
        itemBuilder: (context, index) {
          return _buildNewsCard(newsList[index]);
        },
      ),
    );
  }

  Widget _buildNewsCard(Map<String, String> news) {
    return Card(
      margin: const EdgeInsets.all(8.0),
      elevation: 4.0,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              news['Image']!,
            ),
            Text(
              news['title']!,
              style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 8.0),
            Text(
              news['description']!,
              style: const TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}
