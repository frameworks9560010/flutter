import 'package:flutter/material.dart';

class TraderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Trader'),
      ),
      body: /*Center(
        child: GridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 16.0,
          mainAxisSpacing: 16.0,
          padding: const EdgeInsets.all(16.0),
          children: [
            buildSquareButton(context, "Bidding", '/trader/bidding'),
            buildSquareButton(context, "Winners", '/trader/winners'),
            buildSquareButton(context, "Bid History", '/trader/bidHistory'),
            buildSquareButton(context, "Bill Generate", '/trader/billGenerate'),
            buildSquareButton(context, "Gate Entry", '/trader/gateEntry'),
            buildSquareButton(context, "Logout", '/logout'),
          ],
        ),
      ),
    );
  }*/

          Column(
        children: [
          const SizedBox(height: 30),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/trader/bidding');
            },
            clipBehavior: Clip.antiAlias,
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100.0),
              ),
              padding: EdgeInsets.zero,
            ),
            child: Container(
              height: 100,
              width: double.infinity,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.blue, Colors.white],
                ),
              ),
              child: const Center(
                child: Text(
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25),
                  'Bidding',
                ),
              ),
            ),
          ),
          const SizedBox(height: 30),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/trader/winners');
            },
            clipBehavior: Clip.antiAlias,
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100.0),
              ),
              padding: EdgeInsets.zero,
            ),
            child: Container(
              height: 100,
              width: double.infinity,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.white, Colors.blue],
                ),
              ),
              child: const Center(
                child: Text(
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25),
                  'Winners',
                ),
              ),
            ),
          ),
          const SizedBox(height: 30),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/trader/bidHistory');
            },
            clipBehavior: Clip.antiAlias,
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100.0),
              ),
              padding: EdgeInsets.zero,
            ),
            child: Container(
              height: 100,
              width: double.infinity,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color.fromARGB(255, 68, 243, 33), Colors.white],
                ),
              ),
              child: const Center(
                child: Text(
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25),
                  'History',
                ),
              ),
            ),
          ),
          const SizedBox(height: 30),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/trader/billGenerate');
            },
            clipBehavior: Clip.antiAlias,
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100.0),
              ),
              padding: EdgeInsets.zero,
            ),
            child: Container(
              height: 100,
              width: double.infinity,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Colors.white,
                    Color.fromARGB(255, 68, 243, 33),
                  ],
                ),
              ),
              child: const Center(
                child: Text(
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25),
                  'Bill',
                ),
              ),
            ),
          ),
          const SizedBox(height: 30),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/trader/gateEntry');
            },
            clipBehavior: Clip.antiAlias,
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100.0),
              ),
              padding: EdgeInsets.zero,
            ),
            child: Container(
              height: 100,
              width: double.infinity,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.pink, Colors.white],
                ),
              ),
              child: const Center(
                child: Text(
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25),
                  'Gate Entry',
                ),
              ),
            ),
          ),
          const SizedBox(height: 30),
          ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/');
            },
            clipBehavior: Clip.antiAlias,
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100.0),
              ),
              padding: EdgeInsets.zero,
            ),
            child: Container(
              height: 100,
              width: double.infinity,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.white, Colors.pink],
                ),
              ),
              child: const Center(
                child: Text(
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25),
                  'Exit',
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
