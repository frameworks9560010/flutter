import 'package:flutter/material.dart';

class FarmerPage extends StatelessWidget {
  const FarmerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Farmer'),
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.blue,
              Colors.white,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                buildSquareButton(context, "Lot", '/farmer/lot'),
                const SizedBox(
                  width: 30,
                ),
                buildSquareButton(context, "Lot Status", '/farmer/lotStatus'),
              ],
            ),
            const SizedBox(
              height: 70,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                buildSquareButton(context, "History", '/farmer/lotHistory'),
                const SizedBox(
                  width: 30,
                ),
                buildSquareButton(context, "Bill", '/farmer/bills'),
              ],
            ),
            const SizedBox(
              height: 70,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                buildSquareButton(context, "Gate Entry", '/farmer/gateEntry'),
                const SizedBox(
                  width: 30,
                ),
                buildSquareButton(context, "Exit", '/'),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildSquareButton(BuildContext context, String text, String route) {
    return ElevatedButton(
      onPressed: () {
        Navigator.pushNamed(context, route);
      },
      clipBehavior: Clip.antiAlias,
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(26.0),
        ),
        padding: EdgeInsets.zero,
      ),
      child: Container(
        height: 150,
        width: 170,
        decoration: BoxDecoration(
          gradient: const LinearGradient(
            colors: [
              Color.fromARGB(255, 33, 243, 243),
              Colors.white,
              Color.fromARGB(255, 33, 243, 243)
            ],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          ),
          borderRadius: BorderRadius.circular(26.0),
        ),
        child: Center(
          child: Text(
            text,
            style: const TextStyle(fontSize: 23.0),
          ),
        ),
      ),
    );
  }

  Widget buildCircleButton(BuildContext context, String text, String route) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.pushNamed(context, route);
      },
      child: Text(
        text,
        style: const TextStyle(fontSize: 16.0),
      ),
    );
  }
}
