import 'package:flutter/material.dart';

class FarmerGateEntryPage extends StatelessWidget {
  const FarmerGateEntryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Gate Entry'),
      ),
      body: const Center(
        child: Text('Gate Entry Page for Farmer'),
      ),
    );
  }
}