import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class BiddingDataBase {
  String productName;
  int lotCode;
  String traderName;

  String startDate;
  String endDate;
  num bidPrice;

  BiddingDataBase({
    required this.productName,
    required this.bidPrice,
    required this.endDate,
    required this.lotCode,
    required this.startDate,
    required this.traderName,
  });
  Map<String, dynamic> bidingMap() {
    return {
      'ProductName': productName,
      'BidPrice': bidPrice,
      'EndDate': endDate,
      'StartDate': startDate,
      'LotCode': lotCode,
      'TraderName': traderName,
    };
  }

  @override
  String toString() {
    return '''{
      ProductName:$productName, 
      BidPrice:$bidPrice,
      StartDate:$startDate,
      Enddate:$endDate,
      LotCode:$lotCode,
      TraderName:$traderName
      }''';
  }
}

Future<void> insertData(BiddingDataBase card) async {
  final localDB = await biddingdatabase;
  await localDB.insert(
    'biddingTable',
    card.bidingMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

dynamic biddingdatabase;

dynamic biddingDatabaseFunction() async {
  biddingdatabase = await openDatabase(
    join(await getDatabasesPath(), "BiddingData6.db"),
    version: 1,
    onCreate: (db, version) {
      return db.execute('''CREATE TABLE biddingTable(
        productName TEXT,
        lotCode INTEGER PRIMARY KEY AUTOINCREMENT,
        traderName TEXT,
        startdate TEXT,
        enddate TEXT,
        bidPrice REAL)''');
    },
  );
}
