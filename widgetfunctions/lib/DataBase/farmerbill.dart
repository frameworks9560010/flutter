import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class FarmerBillDataBase {
  int? farmerNo;
  String farmerName;
  String date;
  String place;
  String traderName;
  num totalBill;
  num expenses;
  FarmerBillDataBase({
    required this.date,
    required this.expenses,
    required this.farmerName,
    required this.place,
    required this.totalBill,
    required this.traderName,
  });

  Map<String, dynamic> farmerbillMap() {
    return {
      'FarmerName': farmerName,
      'Date': date,
      'Place': place,
      'Totalbill': totalBill,
      'Expences': expenses,
      'TraderName': traderName,
    };
  }

  @override
  String toString() {
    return '''{
      FarmereName:$farmerName, 
      BidPrice:$date,
      Place:$place,
      Bill:$totalBill,
      expenses:$expenses,
      TraderName:$traderName
      }''';
  }
}

Future<void> insertData(FarmerBillDataBase card) async {
  final localDB = await biddingdatabase;
  await localDB.insert(
    'farmerbillTable',
    card.farmerbillMap(),
    //conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

dynamic biddingdatabase;

dynamic farmerBillDatabaseFunction() async {
  biddingdatabase = await openDatabase(
    join(await getDatabasesPath(), "FarmerBillData3.db"),
    version: 1,
    onCreate: (db, version) {
      return db.execute('''CREATE TABLE farmerbillTable(
        farmerName TEXT,
        expences REAL,
        traderName TEXT,
        date TEXT,
        place TEXT,
        totalBill REAL)''');
    },
  );
}
