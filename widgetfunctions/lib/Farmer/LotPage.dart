import 'package:flutter/material.dart';

class LotPage extends StatefulWidget {
  @override
  _LotPageState createState() => _LotPageState();
}

class _LotPageState extends State<LotPage> {
  String selectedProduct = '[dropdown]';
  String selectedBagType = '[dropdown]';
  DateTime selectedDate = DateTime.now();
  TextEditingController lotCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lot Page'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Lot Code:',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            TextField(
              controller: lotCodeController,
              decoration: InputDecoration(
                hintText: 'Enter lot code',
              ),
            ),
            SizedBox(height: 16.0),
            Text(
              'Lot Date:',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            ElevatedButton(
              onPressed: () => _selectDate(context),
              child: Text('Select Date'),
            ),
            SizedBox(height: 16.0),
            Text(
              'Products:',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            buildDropdown(products, selectedProduct),
            SizedBox(height: 16.0),
            Text(
              'Bag Type:',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            buildDropdown(bagTypes, selectedBagType),
            SizedBox(height: 16.0),
            // Your existing code for number of bags and quantity...
            ElevatedButton(
              onPressed: () {
                // Add your logic for proceeding to bid
                _proceedToBid();
              },
              child: Text('Proceed to Bid'),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildDropdown(List<String> items, String selectedItem) {
    return DropdownButton<String>(
      value: selectedItem,
      onChanged: (String? newValue) {
        setState(() {
          selectedItem = newValue!;
        });
      },
      items: items.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2101),
    );

    if (pickedDate != null && pickedDate != selectedDate) {
      setState(() {
        selectedDate = pickedDate;
      });
    }
  }

  void _proceedToBid() {
    // Add your logic for proceeding to bid
    // This method will be called when the "Proceed to Bid" button is pressed
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Proceed to Bid'),
          content: Text('Your lot information has been submitted for bidding.'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('OK'),
            ),
          ],
        );
      },
    );
  }
}

List<String> products = ['[dropdown]', 'Product A', 'Product B', 'Product C'];
List<String> bagTypes = ['[dropdown]', '30kg', '40kg', '50kg'];
