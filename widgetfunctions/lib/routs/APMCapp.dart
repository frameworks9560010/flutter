import 'package:flutter/material.dart';
import 'package:widgetfunctions/Home/HomePage.dart';
import 'package:widgetfunctions/Home/Profile.dart';
import 'package:widgetfunctions/Home/AboutAPMCPage.dart';
import 'package:widgetfunctions/Home/ContactUsPage.dart';
import 'package:widgetfunctions/Home/FarmerPage.dart';
import 'package:widgetfunctions/Home/LatestNewsPage.dart';
import 'package:widgetfunctions/Home/LiveMarketPage.dart';
import 'package:widgetfunctions/Home/TraderPage.dart';
import 'package:widgetfunctions/Home/WeatherPage.dart';
import 'package:widgetfunctions/Traders/BiddingPage.dart';
import 'package:widgetfunctions/Traders/BidHistoryPage.dart';
import 'package:widgetfunctions/Traders/BillGeneratePage.dart';
import 'package:widgetfunctions/Traders/GateEntryPage.dart';
import 'package:widgetfunctions/Traders/WinnersPage.dart';
import 'package:widgetfunctions/Farmer/Bill.dart';
import 'package:widgetfunctions/Farmer/FarmerGateEntryPage.dart';
import 'package:widgetfunctions/Farmer/LotHistoryPage.dart';
import 'package:widgetfunctions/Farmer/LotStatusPage.dart';
import 'package:widgetfunctions/Farmer/LotPage.dart';
import 'package:widgetfunctions/LoginPage/LoginPage.dart';

class APMCApp extends StatelessWidget {
  const APMCApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'APMC System',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      initialRoute: '/loginPage',
      routes: {
        '/profile': (context) => const Profile(),
        '/loginPage': (context) => const LoginPage(),
        '/': (context) => const HomePage(),
        '/liveMarket': (context) => LiveMarketPage(),
        '/aboutAPMC': (context) => const AboutAPMCPage(),
        '/weather': (context) => const WeatherPage(),
        '/latestNews': (context) => LatestNewsPage(),
        '/trader': (context) => TraderPage(),
        '/farmer': (context) => const FarmerPage(),
        '/contactUs': (context) => ContactUsPage(),
        '/trader/bidding': (context) => const BiddingPage(),
        '/trader/winners': (context) => WinnersPage(),
        '/trader/bidHistory': (context) => BidHistoryPage(),
        '/trader/billGenerate': (context) => const BillGeneratePage(),
        '/trader/gateEntry': (context) => const FarmerGateEntryPage(),
        '/farmer/lot': (context) => LotPage(),
        '/farmer/lotStatus': (context) => const LotStatusPage(),
        '/farmer/lotHistory': (context) => LotHistoryPage(),
        '/farmer/bills': (context) => BillsPage(),
        '/farmer/gateEntry': (context) => const GateEntryPage(),
        //'https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js':(context)=>getnews();
      },
    );
  }
}
