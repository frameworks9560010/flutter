import 'package:flutter/material.dart';
import 'QuizeApp.dart';

void main() => runApp(const QuizeApp());

/*class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: QuizeApp(),
    );
  }
}

class QuizeApp extends StatefulWidget {
  const QuizeApp({super.key});
  @override
  createState() {
    return _QuizeAppState();
  }
}

class _QuizeAppState extends State {
  bool isQuestionScreen = true;
  int questionNo = 0;
  int isPressed = -1;
  int correctAns = 0;

  List<Map> questions = [
    {
      'q': 'Who is the founder of Microsoft?',
      'options': ['Steve jobs', 'Jeff Bezos', 'Bill Gates', 'Larry Page'],
      'answer': 3,
    },
    {
      'q': 'Who is the founder of Apple?',
      'options': ['Steve jobs', 'Jeff Bezos', 'Bill Gates', 'Larry Page'],
      'answer': 1,
    },
    {
      'q': 'Who is the founder of Amazon?',
      'options': ['Steve jobs', 'Jeff Bezos', 'Bill Gates', 'Larry Page'],
      'answer': 2,
    },
    {
      'q': 'Who is the founder of Google?',
      'options': ['Steve jobs', 'Jeff Bezos', 'Bill Gates', 'Larry Page'],
      'answer': 4,
    },
    {
      'q': 'Who is the founder of Tesla?',
      'options': ['Elone Musk', 'Jeff Bezos', 'Bill Gates', 'Larry Page'],
      'answer': 1,
    },
  ];

  MaterialStateProperty<Color?> buttonColor(int optionNo) {
    if (isPressed != -1) {
      if (optionNo == questions[questionNo]['answer']) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (isPressed == optionNo) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(null);
      }
    } else {
      return const MaterialStatePropertyAll(null);
    }
  }

  SizedBox options(String option, int optionNo) {
    return SizedBox(
      height: 50,
      width: 300,
      child: ElevatedButton(
        onPressed: () {
          if (isPressed == -1) {
            setState(() {
              isPressed = optionNo;
            });
          }
        },
        style: ButtonStyle(
          backgroundColor: buttonColor(optionNo),
        ),
        child: Text(option),
      ),
    );
  }

  void pageChake() {
    if (isPressed == -1) {
      Tooltip(
        message: "please attend questionNo ${questionNo + 1} question!!!",
      );
      return;
    }

    if (isPressed == questions[questionNo]['answer']) {
      correctAns += 1;
    }

    if (questionNo == questions.length - 1) {
      isQuestionScreen = false;
      return;
    }

    isPressed = -1;
    questionNo += 1;
  }

  Scaffold isScreen() {
    if (isQuestionScreen) {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Quize App',
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.black,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.blue,
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Question :- ${questionNo + 1}/${questions.length}",
                  style: const TextStyle(fontSize: 20),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  questions[questionNo]['q'],
                  style: const TextStyle(fontSize: 18),
                ),
              ],
            ),
            const SizedBox(height: 40),
            options(questions[questionNo]['options'][0], 1),
            const SizedBox(height: 40),
            options(questions[questionNo]['options'][1], 2),
            const SizedBox(height: 40),
            options(questions[questionNo]['options'][2], 3),
            const SizedBox(height: 40),
            options(questions[questionNo]['options'][3], 4),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            pageChake();
            setState(() {});
          },
          child: const Text("Next", style: TextStyle(fontSize: 20)),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Quize App',
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.black,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.blue,
        ),
        body: Column(
          children: [
            Image.network(
              'https://previews.123rf.com/images/macrovector/macrovector1504/macrovector150400777/38995744-winner-cup-with-gold-medal-and-star-with-ribbon-on-the-white-background-vector-illustration.jpg',
              height: 450,
            ),
            const Text(
              'Congratulations!!! ',
              style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.w700,
                  color: Colors.blue),
            ),
            const Text(
              'you have completed Quize!',
              style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
            ),
            Text(
              'Score : $correctAns/${questions.length}',
              style: const TextStyle(
                  fontSize: 20,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 30),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  isQuestionScreen = true;
                  questionNo = 0;
                  isPressed = -1;
                  correctAns = 0;
                });
              },
              child: const Text('Reset'),
            )
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isScreen();
  }
}*/
