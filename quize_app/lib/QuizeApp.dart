import 'package:flutter/material.dart';

class QuizeApp extends StatefulWidget {
  const QuizeApp({super.key});

  @override
  State<QuizeApp> createState() => _MyAppState();
}

class QuestionModel {
  final String? question;
  final List<String>? options;
  final int? ans;

  const QuestionModel({this.question, this.options, this.ans});
}

class _MyAppState extends State<QuizeApp> {
  bool isQuestionScreen = true;
  int questionNo = 0;
  int onPressed = -1;
  int correctAns = 0;

  List Q = [
    const QuestionModel(
      question: 'Which of the following is not the type of queue?',
      options: [
        ' Priority Queue',
        'Circular Queue',
        'Double Ended Queue',
        'Singly Linked List'
      ],
      ans: 3,
    ),
    const QuestionModel(
      question: 'Which of the following highly uses the concept of an array?',
      options: ['Hashing', 'Sorting', 'Searching', 'All of the above'],
      ans: 3,
    ),
    const QuestionModel(
      question:
          'What is the worst case time complexity of linear search algorithm?',
      options: [' O(n)', 'O(log n)', 'O(1)', 'O(n^2)'],
      ans: 0,
    ),
    const QuestionModel(
      question:
          'Which of the following is an example of dynamic programming approach?',
      options: [
        'Greedy Algorithm',
        ' Divide and Conquer',
        ' Backtracking',
        ' All of the above'
      ],
      ans: 3,
    ),
    const QuestionModel(
      question:
          'Which of the following data structure is used for breadth first traversal of a graph?',
      options: ['Stack', 'Queue', 'LinkedList', 'Tree'],
      ans: 1,
    ),
  ];

  MaterialStateProperty<Color?> color(int option) {
    if (onPressed != -1) {
      if (option == Q[questionNo].ans) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (onPressed == option) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(null);
      }
    } else {
      return const MaterialStatePropertyAll(null);
    }
  }

  SizedBox button(int option) {
    return SizedBox(
      height: 50,
      width: 200,
      child: ElevatedButton(
        onPressed: () {
          if (onPressed == -1) {
            onPressed = option;
            setState(
              () {},
            );
          }
        },
        style: ButtonStyle(
          backgroundColor: color(option),
        ),
        child: Text(Q[questionNo].options[option]),
      ),
    );
  }

  void pageChake() {
    if (onPressed == -1) {
      return;
    } else {
      if (Q[questionNo].ans == onPressed) {
        correctAns += 1;
      }
      if (Q.length - 1 == questionNo) {
        isQuestionScreen = false;
      }
      onPressed = -1;
      questionNo += 1;
    }
    setState(() {});
  }

  Scaffold onScreen() {
    if (isQuestionScreen) {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 14, 129, 223),
          title: const Text(
            'QuizApp',
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
          ),
        ),
        body: Container(
          height: 550,
          margin: const EdgeInsets.only(top: 1),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(80),
                bottomRight: Radius.circular(80)),
            color: Color.fromARGB(255, 225, 234, 234),
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 220, 195, 225),
                offset: Offset(10, 30),
                blurRadius: 18,
              ),
              BoxShadow(
                color: Color.fromARGB(255, 189, 170, 193),
                offset: Offset(10, 30),
                blurRadius: 18,
              ),
            ],
          ),
          child: Column(
            children: [
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Question :- ${questionNo + 1}/${Q.length}',
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.all(20),
                child: Text(
                  Q[questionNo].question,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              const SizedBox(height: 25),
              button(0),
              const SizedBox(height: 25),
              button(1),
              const SizedBox(height: 25),
              button(2),
              const SizedBox(height: 25),
              button(3),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            pageChake();
          },
          child: const Text('Next'),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          title: const Text(
            'QuizApp',
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
          ),
        ),
        body: Column(
          children: [
            Image.network(
              'https://previews.123rf.com/images/macrovector/macrovector1504/macrovector150400777/38995744-winner-cup-with-gold-medal-and-star-with-ribbon-on-the-white-background-vector-illustration.jpg',
              height: 450,
            ),
            const Text(
              'Congratulations!!! ',
              style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.w700,
                  color: Colors.blue),
            ),
            const Text(
              'you have completed Quize!',
              style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
            ),
            Text(
              'Score : $correctAns/${Q.length}',
              style: const TextStyle(
                  fontSize: 20,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 30),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  isQuestionScreen = true;
                  questionNo = 0;
                  onPressed = -1;
                  correctAns = 0;
                });
              },
              child: const Text('Reset'),
            )
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: onScreen(),
    );
  }
}
