/*import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class ModelClass {
  final String title;
  final String description;
  final String date;
  const ModelClass({
    required this.title,
    required this.description,
    required this.date,
  });
}

class ToDoDatabase {
  int? cardNo;
  String title;
  String description;
  String date;
  ToDoDatabase({
    this.cardNo,
    required this.title,
    required this.description,
    required this.date,
  });
  Map<String, dynamic> todoMap() {
    return {
      'title': title,
      'description': description,
      'date': date,
    };
  }

  @override
  String toString() {
    return '''{
      cardNo:$cardNo, 
      title:$title,
      description:$description,
      date:$date
      }''';
  }
}

Future<void> insertData(ToDoDatabase card) async {
  final localDB = await database;
  await localDB.insert(
    'ToDoTable',
    card.todoMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<void> deleteCard(int? cardNo) async {
  final localDB = await database;
  await localDB.delete('ToDoTable', where: "cardNo = ?", whereArgs: [cardNo]);
  print('.............in delete function .................');
}

Future<void> updateCard(ToDoDatabase obj) async {
  final localDB = await dataBase();
  await localDB.update('ToDoTable', obj.todoMap(),
      print('in update ................................'),
      where: 'cardNo = ?', whereArgs: [obj.cardNo]);
}

dynamic database;
// dynamic dataBase() async {
//   database = await openDatabase(join(await getDatabasesPath(), "DataBase11.db"),
//       version: 1, onCreate: (db, version) {
//     return db.execute('''CREATE TABLE ToDoTable(
// 	cardNo INTEGER PRIMARY KEY AUTOINCREMENT,
// 	title TEXT,
// 	description TEXT,
// 	date TEXT
// 	)''');
//   });

//await getData();
//}
dynamic dataBase() async {
  database = await openDatabase(join(await getDatabasesPath(), "DataBase11.db"),
      version: 1, onCreate: (db, version) {
    return db.execute('''CREATE TABLE ToDoTable(
        cardNo INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT,
        description TEXT,
        date TEXT
      )''');
  });
}*/

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class ModelClass {
  final String title;
  final String description;
  final String date;
  const ModelClass({
    required this.title,
    required this.description,
    required this.date,
  });
}

class ToDoDatabase {
  int? cardNo; // This should not be set when creating a new object
  String title;
  String description;
  String date;
  ToDoDatabase({
    //this.cardNo, // Remove this from the constructor
    required this.title,
    required this.description,
    required this.date,
  });
  Map<String, dynamic> todoMap() {
    return {
      'title': title,
      'description': description,
      'date': date,
    };
  }

  @override
  String toString() {
    return '''{
      cardNo:$cardNo, 
      title:$title,
      description:$description,
      date:$date
      }''';
  }
}

Future<void> insertData(ToDoDatabase card) async {
  final localDB = await database;
  await localDB.insert(
    'ToDoTable',
    card.todoMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<void> deleteCard(int? cardNo) async {
  final localDB = await database;
  await localDB.delete('ToDoTable', where: "cardNo = ?", whereArgs: [cardNo]);
  print('.............in delete function .................');
}

Future<void> updateCard(ToDoDatabase obj) async {
  final localDB = await dataBase();
  await localDB.update('ToDoTable', obj.todoMap(),
      where: 'cardNo = ?', whereArgs: [obj.cardNo]);
  print('in update ................................');
}

dynamic database;
Future<Database> dataBase() async {
  database = await openDatabase(join(await getDatabasesPath(), "DataBase13.db"),
      version: 1, onCreate: (db, version) {
    return db.execute('''CREATE TABLE ToDoTable(
	cardNo INTEGER PRIMARY KEY AUTOINCREMENT,
	title TEXT,
	description TEXT,
	date TEXT
	)''');
  });

  return database; // Return the database
}
