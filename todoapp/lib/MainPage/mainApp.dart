import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:todoapp/DataBase/sqdata.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});
  @override
  State<StatefulWidget> createState() => MainPageState();
}

class MainPageState extends State {
  Future<List<ToDoDatabase>> getData() async {
    final localDB = await database;
    List<Map<String, dynamic>> mapEntry = await localDB.query("ToDoTable");

    return List.generate(mapEntry.length, (i) {
      setState(() {});
      return ToDoDatabase(
          title: mapEntry[i]['title'],
          description: mapEntry[i]['description'],
          date: mapEntry[i]['date']);
    });
  }

  List<ToDoDatabase> data = [];
  void card1() async {
    await dataBase();

    data = await getData();
    print(data);
  }

  @override
  initState() {
    super.initState();

    card1();
  }

  void getdataBase(ToDoDatabase obj) async {
    data = (await getData());
    //print(await DataFetch.getData());
    setState(() {});
  }

  TextEditingController title = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController date = TextEditingController();
  Padding textfield(TextEditingController controller, String texttype) {
    if (texttype == 'date') {
      return Padding(
        padding: const EdgeInsets.only(left: 15, right: 15, bottom: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              texttype,
              style: GoogleFonts.inter(
                  color: const Color.fromRGBO(0, 0, 0, 0.7),
                  fontWeight: FontWeight.w400,
                  fontSize: 15),
            ),
            TextField(
              controller: controller,
              onTap: () async {
                DateTime? pickedDate = await showDatePicker(
                  context: context,
                  firstDate: DateTime(1900),
                  lastDate: DateTime(2100),
                );
                String formatedDate = DateFormat.yMMMd().format(pickedDate!);
                setState(() {
                  controller.text = formatedDate;
                });
              },
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.only(left: 15, right: 15, bottom: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              texttype,
              style: GoogleFonts.inter(
                  color: const Color.fromRGBO(0, 0, 0, 0.7),
                  fontWeight: FontWeight.w400,
                  fontSize: 15),
            ),
            TextField(
              controller: controller,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  void sheet(bool edit) {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
        ),
        context: context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 15, bottom: 15),
                  child: Center(
                    child: Text(
                      'Create To-Do',
                      style: GoogleFonts.inter(
                          color: const Color.fromRGBO(0, 0, 0, 1),
                          fontWeight: FontWeight.w600,
                          fontSize: 25),
                    ),
                  ),
                ),
                textfield(title, 'title'),
                textfield(description, 'description'),
                textfield(date, 'date'),
                Center(
                  child: ElevatedButton(
                    onPressed: () async {
                      ToDoDatabase obj = ToDoDatabase(
                          title: title.text,
                          description: description.text,
                          date: date.text);
                      if (edit) {
                        await updateCard(obj);
                      } else {
                        await insertData(obj);
                      }
                      getdataBase(obj);
                      print(await getData());

                      setState(() {
                        Navigator.pop(context);
                        title.clear();
                        date.clear();
                        description.clear();
                      });
                    },
                    child: Text(
                      'Submit',
                      style: GoogleFonts.inter(
                          color: const Color.fromARGB(255, 71, 69, 190),
                          fontWeight: FontWeight.w600,
                          fontSize: 18),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                )
              ],
            ),
          );
        });
  }

  Widget card(int index) {
    return Slidable(
      key: const ValueKey(1),
      endActionPane: ActionPane(
        motion: const DrawerMotion(),
        dismissible: DismissiblePane(
          onDismissed: () {
            deleteCard(data[index].cardNo);
          },
          closeOnCancel: true,
        ),
        children: [
          SlidableAction(
            onPressed: (context) {
              title.text = data[index].title;
              description.text = data[index].description;
              date.text = data[index].date;
              sheet(true);
            },
            backgroundColor: Color.fromARGB(255, 165, 173, 218),
            foregroundColor: Colors.white,
            icon: Icons.edit,
            label: 'edit',
          ),
          SlidableAction(
            onPressed: (context) async {
              await deleteCard(data[index].cardNo);
              setState(() {});
              print(await getData());
            },
            backgroundColor: const Color(0xFFFE4A49),
            foregroundColor: Colors.white,
            icon: Icons.delete,
            label: 'Delete',
          ),
        ],
      ),
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: const BoxDecoration(
            color: Color.fromARGB(255, 248, 249, 250),
            boxShadow: [
              BoxShadow(
                offset: Offset(2, 128),
                blurRadius: 20,
                color: Color.fromRGBO(255, 255, 255, 0.7),
              ),
            ]),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 15, left: 8),
              child: ClipOval(
                child: SizedBox.fromSize(
                  size: const Size.fromRadius(40), // Image radius
                  child: Image.asset('assets/shraddha.jpeg', fit: BoxFit.cover),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Text(
                    data[index].title,
                    style: GoogleFonts.inter(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontSize: 18),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 8),
                  width: 282,
                  child: Text(
                    data[index].description,
                    style: GoogleFonts.inter(
                        color: const Color.fromRGBO(0, 0, 0, 0.7),
                        fontWeight: FontWeight.w400,
                        fontSize: 15),
                  ),
                ),
                Text(
                  data[index].date,
                  style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(0, 0, 0, 0.7),
                      fontWeight: FontWeight.w400,
                      fontSize: 15),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //setState(() {});
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Color.fromRGBO(111, 81, 255, 1),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 75, left: 30),
              child: Text(
                'Good Morning',
                style: GoogleFonts.quicksand(
                  fontSize: 25,
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(255, 255, 255, 1),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30, bottom: 31),
              child: Text(
                'Pathum',
                style: GoogleFonts.quicksand(
                  fontSize: 35,
                  fontWeight: FontWeight.w600,
                  color: const Color.fromRGBO(255, 255, 255, 1),
                ),
              ),
            ),
            Container(
              height: 707.4,
              width: 411,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(217, 217, 217, 1),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                    child: Text(
                      'CREATE TO DO LIST',
                      style: GoogleFonts.quicksand(
                          fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(
                    height: 654,
                    width: 411,
                    decoration: const BoxDecoration(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                    child: ListView.builder(
                      itemCount: data.length,
                      itemBuilder: (BuildContext context, index) {
                        return card(index);
                      },
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          sheet(false);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
