import 'package:flutter/material.dart';
import 'package:todoapp/loginPage/signingPage.dart';

class TodoApp extends StatefulWidget {
  const TodoApp({super.key});

  @override
  State createState() => _TodoAppState();
}

class _TodoAppState extends State<TodoApp> {
  TextEditingController username = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  final GlobalKey _formKey = GlobalKey();

  Widget input(String hint, TextEditingController ctr) {
    print('in input');
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: TextFormField(
        controller: ctr,
        decoration: InputDecoration(
          hintText: hint,
          border: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
              width: 5,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
              width: 1,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          prefixIcon: const Icon(Icons.person),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(color: Colors.blueAccent),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 100),
              child: ClipOval(
                child: SizedBox.fromSize(
                  size: const Size.fromRadius(60), // Image radius
                  child: Image.asset('assets/shraddha.jpeg', fit: BoxFit.cover),
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 30, left: 70, right: 50),
              child: SizedBox(
                child: Text(
                  'Please Login in your existing Account',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  input('user name', username),
                  input('email', email),
                  input('password', password),
                  ElevatedButton(
                      onPressed: () async {
                        LoginDatabase obj = LoginDatabase(
                            userName: username.text,
                            email: email.text,
                            passWord: password.text);
                        await insertData(obj);
                        print('.........................');
                        print(await getLoginData());
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const signingPage()),
                        );
                      },
                      child: const Text('Login'))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
