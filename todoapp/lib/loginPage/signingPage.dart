import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todoapp/MainPage/mainApp.dart';



class signingPage extends StatefulWidget {
  const signingPage({super.key});

  @override
  State createState() => _TodoAppState();
}



dynamic logindatabase;

class _TodoAppState extends State<signingPage> {
  List<Map<String, dynamic>> usr = [];
  TextEditingController username = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  final GlobalKey _formKey = GlobalKey();

  
   

 

  Widget input(String hint, TextEditingController ctr) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: TextFormField(
        controller: ctr,
        decoration: InputDecoration(
          hintText: hint,
          border: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
              width: 5,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black,
              width: 1,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          prefixIcon: const Icon(Icons.person),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(color: Colors.blueAccent),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 100),
              child: ClipOval(
                child: SizedBox.fromSize(
                  size: const Size.fromRadius(60), // Image radius
                  child: Image.asset('assets/shraddha.jpeg', fit: BoxFit.cover),
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 30, left: 70, right: 50),
              child: SizedBox(
                child: Text(
                  'Please Login in your existing Account',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  input('user name', username),
                  input('password', password),
                  ElevatedButton(
                      onPressed: () async {
                       
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const MainPage()),
                        );
                      },
                      child: const Text('Login'))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


Future<void> insertData(LoginDatabase card) async {
    final localDB = await logindatabase;
    await localDB.insert(
      'USER',
      card.todoMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
  Future<List<LoginDatabase>> getLoginData() async {
    final localDB = await logindatabase;
    List<Map<String, dynamic>> log = await localDB.query('USER');
    return List.generate(log.length, (index) {
      return LoginDatabase(
          userName: log[index]['userName'],
          email: log[index]['email'],
          passWord: log[index]['passWord']);
    });
  }
class LoginDatabase {
  String userName;
  String email;
  String passWord;
  LoginDatabase({
    required this.userName,
    required this.email,
    required this.passWord,
  });
  Map<String, dynamic> todoMap() {
    return {
      'userName': userName,
      'email': email,
      'passWord': passWord,
    };
  }

  @override
  String toString() {
    return '{ userName:$userName,email:$email,passWord:$passWord}';
  }
}
 dynamic logindataBase() async {
    logindatabase = await openDatabase(
      join(await getDatabasesPath(), "loginDataBase1.db"),
      version: 1,
      onCreate: (db, version) {
        return db.execute('''CREATE TABLE USER(
        userName TEXT PRIMARY KEY,
        email TEXT,
        passWord TEXT
        )''');
      },
    );
  }