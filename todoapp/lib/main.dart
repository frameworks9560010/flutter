import 'package:flutter/material.dart';
import 'package:todoapp/loginPage/signingPage.dart';
import 'App.dart';
import 'package:todoapp/DataBase/sqdata.dart';

dynamic database;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dataBase();
  await logindataBase();
  runApp(const App());
}
