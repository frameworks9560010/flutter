import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:url_launcher/url_launcher_string.dart';

class ContactList extends StatefulWidget {
  const ContactList({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ContactListState();
  }
}

class _ContactListState extends State {
  List<Contact> contactsinfo = [];

  void getpermission() async {
    if (await Permission.contacts.isGranted) {
      contactsinfo = await ContactsService.getContacts();
      setState(() {});
    } else {
      await Permission.phone.request();
      await Permission.contacts.request();
      await Permission.phone.request();
    }
  }

  void makePhoneCall(String phoneNumber) async {
    String cleanedPhoneNumber = phoneNumber.replaceAll(RegExp(r'[^\d]'), '');

    if (cleanedPhoneNumber.isNotEmpty) {
      if (await canLaunchUrlString('tel:$cleanedPhoneNumber')) {
        await launchUrlString('tel:$cleanedPhoneNumber');
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getpermission();
  }

  showContactDialog(int index) async {
    return await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Center(
            child: Text(
              'Contact',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
            ),
          ),
          content: SizedBox(
            height: MediaQuery.of(context).size.height / 6,
            child: Column(
              children: [
                Text(
                  '${contactsinfo[index].displayName}',
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    contactsinfo[index].birthday != null
                        ? 'Birth Day:- ${contactsinfo[index].birthday}'
                        : 'BirthDay not found',
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    contactsinfo[index].phones!.isNotEmpty
                        ? 'Mo:- ${contactsinfo[index].phones!.first.value}'
                        : 'no number found',
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Text(
                  contactsinfo[index].emails!.isNotEmpty
                      ? 'Email:- ${contactsinfo[index].emails!.first.value}'
                      : 'no email found',
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          ),
          actions: [
            Center(
              child: ElevatedButton(
                onPressed: () {
                  if (contactsinfo[index].phones!.isNotEmpty) {
                    makePhoneCall('${contactsinfo[index].phones!.first.value}');
                  } else {}
                },
                child: const Text('Call'),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget card(int index) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: GestureDetector(
        onTap: () async {
          showContactDialog(index);
        },
        child: Container(
          height: 100,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 145, 198, 241),
              border: Border(
                top: BorderSide(color: Colors.black),
                bottom: BorderSide(color: Colors.black),
                left: BorderSide(color: Colors.black),
                right: BorderSide(color: Colors.black),
              ),
              borderRadius: BorderRadius.all(Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(255, 208, 216, 214),
                  blurRadius: 15,
                  spreadRadius: 2,
                  offset: Offset(4, 4),
                )
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(
                  '${contactsinfo[index].displayName}',
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.w500),
                ),
              ),
              Text(
                contactsinfo[index].phones!.isNotEmpty
                    ? '${contactsinfo[index].phones!.first.value}'
                    : '',
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Contacts',
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
        ),
      ),
      body: ListView.builder(
        itemCount: contactsinfo.length,
        itemBuilder: (context, index) {
          return card(index);
        },
      ),
    );
  }
}
