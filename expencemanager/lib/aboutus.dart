import 'package:expencemanager/drawer.dart';
import 'package:flutter/material.dart';

class AboutClass extends StatefulWidget {
  const AboutClass({super.key});

  @override
  State createState() => _AboutState();
}

class _AboutState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'About Us',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        ),
      ),
      drawer: const DrawerClass(),
    );
  }
}
