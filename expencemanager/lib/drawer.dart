import 'package:expencemanager/aboutus.dart';
import 'package:expencemanager/category.dart';
import 'package:expencemanager/graph.dart';
import 'package:expencemanager/home.dart';
import 'package:expencemanager/trash.dart';

import 'package:flutter/material.dart';

class DrawerClass extends StatefulWidget {
  const DrawerClass({super.key});

  @override
  State createState() => _DrawerState();
}

class _DrawerState extends State {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: MediaQuery.of(context).size.width / 2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 15.0),
            child: Text(
              'Expense Manager',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(left: 15.0),
            child: Text(
              'Save your transactions',
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const HomePage(),
                  ),
                );
              },
              child: const Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 8),
                    child: Icon(
                      Icons.calendar_month,
                      color: Colors.green,
                    ),
                  ),
                  Text(
                    'Transaction',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const GraphClass(),
                  ),
                );
              },
              child: const Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 8),
                    child: Icon(
                      Icons.graphic_eq_rounded,
                      color: Colors.green,
                    ),
                  ),
                  Text(
                    'Graphs',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CategoryClass(),
                  ),
                );
              },
              child: const Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 8),
                    child: Icon(
                      Icons.category_sharp,
                      color: Colors.green,
                    ),
                  ),
                  Text(
                    'Category',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const TrashClass(),
                  ),
                );
              },
              child: const Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 8),
                    child: Icon(
                      Icons.delete,
                      color: Colors.green,
                    ),
                  ),
                  Text(
                    'Trash',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const AboutClass(),
                  ),
                );
              },
              child: const Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 8),
                    child: Icon(
                      Icons.person,
                      color: Colors.green,
                    ),
                  ),
                  Text(
                    'About us',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
