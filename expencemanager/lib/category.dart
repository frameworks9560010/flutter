import 'package:expencemanager/drawer.dart';
import 'package:flutter/material.dart';

class CategoryClass extends StatefulWidget {
  const CategoryClass({super.key});

  @override
  State createState() => _CategoryState();
}

class _CategoryState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Category',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        ),
      ),
      drawer: const DrawerClass(),
      body: GridView.builder(
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              width: MediaQuery.of(context).size.width / 2,
              height: MediaQuery.of(context).size.height / 4,
              decoration: const BoxDecoration(
                color: Color.fromARGB(255, 238, 233, 233),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Image.asset('assets/Group.png'),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: Text('Food'),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
