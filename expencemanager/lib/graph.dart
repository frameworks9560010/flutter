import 'package:expencemanager/drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pie_chart/pie_chart.dart';

class GraphClass extends StatefulWidget {
  const GraphClass({super.key});

  @override
  State createState() => _GraphState();
}

class _GraphState extends State {
  Map<String, double> map = {
    'daru': 50,
    'Sutta': 20,
    'Ganja': 20,
    'Chakana': 10
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Graph',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        ),
      ),
      drawer: const DrawerClass(),
      body: Container(
        height: double.infinity,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 25, left: 20),
              child: PieChart(
                dataMap: map,
                chartType: ChartType.ring,
                chartRadius: 200,
                ringStrokeWidth: 40,
                //
                centerWidget: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Total'),
                  ],
                ),
                legendOptions:
                    const LegendOptions(legendShape: BoxShape.rectangle),
                chartValuesOptions:
                    const ChartValuesOptions(showChartValues: false),
              ),
            ),
            Expanded(
              child: Container(
                height: 500,
                child: ListView.separated(
                  itemCount: 5,
                  itemBuilder: ((context, index) {
                    return Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 30, left: 15, right: 20),
                          child: Container(
                            height: 70,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle, color: Colors.red),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.asset(
                                'assets/Group.png',
                                height: 20,
                              ),
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            'Food',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    );
                  }),
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(height: 0);
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
