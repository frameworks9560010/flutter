import 'package:expencemanager/login.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State createState() {
    return _SignUpState();
  }
}

class _SignUpState extends State {
  TextEditingController name = TextEditingController();
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController cpassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/Group.png'),
            const Padding(
              padding: EdgeInsets.all(10.0),
              child: Text(
                'Create your Account',
                style: TextStyle(
                  fontSize: 19,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    //spreadRadius: 5,
                    blurRadius: 2,
                    offset: const Offset(0, 3),
                  ),
                ]),
                child: TextField(
                  controller: name,
                  decoration: InputDecoration(
                      labelText: 'name',
                      constraints: BoxConstraints(
                        maxWidth: screen.width / 1.5,
                      ),
                      border: InputBorder.none),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: TextField(
                controller: name,
                decoration: InputDecoration(
                  labelText: 'username',
                  constraints: BoxConstraints(
                    maxWidth: screen.width / 1.5,
                  ),
                  border: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: TextField(
                controller: name,
                decoration: InputDecoration(
                  labelText: 'password',
                  constraints: BoxConstraints(
                    maxWidth: screen.width / 1.5,
                  ),
                  border: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: TextField(
                controller: name,
                decoration: InputDecoration(
                  labelText: 'confirm password',
                  constraints: BoxConstraints(
                    maxWidth: screen.width / 1.5,
                  ),
                  border: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const Login(),
                    ),
                  );
                },
                style: const ButtonStyle(
                  // shape: MaterialStatePropertyAll(),
                  backgroundColor: MaterialStatePropertyAll(
                    Color.fromRGBO(14, 161, 125, 1),
                  ),
                  // minimumSize: MaterialStatePropertyAll(screen / 9),
                  //fixedSize: MaterialStatePropertyAll(screen / 8),
                ),
                child: const Text(
                  'Sign Up',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w400),
                ),
              ),
            ),
            const Spacer(),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Already have an account?'),
                  TextButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const Login(),
                          ),
                        );
                      },
                      child: const Text('sign in')),
                ],
              ),
            )
          ],
        ),
      ),
     
    );
  }
}
