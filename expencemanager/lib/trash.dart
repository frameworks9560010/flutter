import 'package:expencemanager/drawer.dart';
import 'package:flutter/material.dart';

class TrashClass extends StatefulWidget {
  const TrashClass({super.key});

  @override
  State createState() => _TrashState();
}

class _TrashState extends State {
  Widget card() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        children: [
          Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Color.fromRGBO(97, 119, 109, 0.698),
            ),
            child: Image.asset(
              'assets/Vector.png',
              height: 40,
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  child: Text(
                    'Medicine',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(
                  child: Text(
                    'lorem simple dummy dataa',
                    style: TextStyle(fontSize: 13),
                  ),
                ),
              ],
            ),
          ),
          const Column(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.remove_circle_rounded,
                    color: Color.fromARGB(255, 88, 85, 85),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      '500',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              ),
              Text('3 june|11.50 Am'),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Trash',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        ),
      ),
      drawer: const DrawerClass(),
      body: ListView.builder(
        itemCount: 5,
        itemBuilder: (context, index) {
          return card();
        },
      ),
    );
  }
}
