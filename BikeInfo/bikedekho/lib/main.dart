import 'package:bikedekho/Data/model.dart';
import 'package:bikedekho/Data/showBikeData.dart';
import 'package:bikedekho/Screens/brand.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dataBase();
  await aviDatabase();
  runApp(const BikeDekho());
}
