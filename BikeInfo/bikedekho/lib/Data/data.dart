import 'package:bikedekho/Screens/showbike.dart';

class BrandModel {
  final String image;
  final String brand;
  final Showbike bike;
  BrandModel({required this.brand, required this.image, required this.bike});
}

List<BrandModel> brandList = [
  BrandModel(
    bike: const Showbike(),
    brand: 'Yamaha',
    image:
        'https://i.pinimg.com/originals/4f/f5/09/4ff509a7f661b99574cb6b0d86e14232.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Royal Enfield',
    image:
        'https://logolook.net/wp-content/uploads/2022/10/Royal-Enfield-Logo.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'TVS',
    image:
        'https://www.freepnglogos.com/uploads/tvs-logo-png/tvs-motors-logo-png-0.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Bajaj',
    image: 'https://1000logos.net/wp-content/uploads/2020/06/Bajaj-Logo.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Honada',
    image:
        'https://e7.pngegg.com/pngimages/444/697/png-clipart-honda-logo-honda-car-scooter-motorcycle-yamaha-motor-company-honda-logo-red-angle-text-thumbnail.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Hero',
    image:
        'https://download.logo.wine/logo/Hero_MotoCorp/Hero_MotoCorp-Logo.wine.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Suzuki',
    image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Suzuki_Motor_Corporation_logo.svg/1280px-Suzuki_Motor_Corporation_logo.svg.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'KTM',
    image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/KTM-Logo.svg/1280px-KTM-Logo.svg.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Jawa',
    image:
        'https://logos-world.net/wp-content/uploads/2022/12/Jawa-Moto-Logo-1931.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Dukati',
    image: 'https://logos-world.net/wp-content/uploads/2021/03/Ducati-Logo.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Kawasaki',
    image:
        'https://i.pinimg.com/736x/f2/30/29/f23029e9d510c1a886f4f3df3a816a22.jpg',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Benelli',
    image: 'https://1000logos.net/wp-content/uploads/2020/07/Benelli-logo.jpg',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Triumph',
    image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Logo_Triumph.svg/1280px-Logo_Triumph.svg.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Indian',
    image:
        'https://seeklogo.com/images/I/indian-motorcycle-logo-66ABD72B21-seeklogo.com.png',
  ),
  BrandModel(
      bike: const Showbike(),
      brand: 'BMW',
      image:
          'https://www.shutterstock.com/image-vector/lviv-ukraine-may-17-2023-260nw-2304791043.jpg'),
  BrandModel(
    bike: const Showbike(),
    brand: 'Ather',
    image:
        'https://zeevector.com/wp-content/uploads/Ather_Energy_Logo-PNG@.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Revolt',
    image: 'https://www.revoltmotors.com/images/logo-black-large.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Okinawa',
    image:
        'https://upload.wikimedia.org/wikipedia/commons/c/c9/OkinawaLogo.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Benling India',
    image:
        'https://www.benlingindia.com/wp-content/uploads/2020/09/logoblack125_89-min-1.png',
  ),
  BrandModel(
    bike: const Showbike(),
    brand: 'Avon',
    image: 'https://lsmedia.linker-cdn.net/276716/2021/5849356.jpeg',
  ),
];
