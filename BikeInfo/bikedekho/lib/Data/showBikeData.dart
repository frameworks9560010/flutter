import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class Bikes {
  String brand;
  String image;
  String bikeType;
  String price;
  Bikes(
      {required this.bikeType,
      required this.brand,
      required this.image,
      required this.price});
  Map<String, dynamic> bikes() {
    return {
      'brand': brand,
      'image': image,
      'bikeType': bikeType,
      'price': price,
    };
  }

  @override
  String toString() {
    return '{brand:$brand, image:$image, bikeType:$bikeType,price:$price}';
  }
}

//Inserting DataBase
Future<void> insertBike(Bikes obj) async {
  final localDB = await database;
  await localDB.insert(
    'SHOWBIKE',
    obj.bikes(),
  );
  print('***************');
}

//opning DataBase
dynamic database;
dataBase() async {
  database = await openDatabase(
    join(await getDatabasesPath(), 'BikeDataBase22.db'),
    version: 1,
    onCreate: (db, version) {
      return db.execute('''CREATE TABLE SHOWBIKE(
        bikeType TEXT PRIMARY KEY,
        image TEXT,
        brand TEXT,
        price TEXT
      )''');
    },
  );
  Bikes mt15 = Bikes(
    bikeType: 'MT15',
    brand: 'Yamaha',
    price: 'Rs1.68 - 1.73 Lakh*',
    image:
        'https://bd.gaadicdn.com/processedimages/yamaha/mt-15-2-0/640X309/mt-15-2-063ea25b00fa58.jpg',
  );
  await insertBike(mt15);
  print('.................................0');
  Bikes mt16 = Bikes(
    bikeType: 'MT16',
    brand: 'Yamaha',
    price: 'Rs2.68 - 2.73 Lakh*',
    image:
        'https://bd.gaadicdn.com/processedimages/yamaha/mt-15-2-0/640X309/mt-15-2-063ea25b00fa58.jpg',
  );
  await insertBike(mt16);
  print('.................................0');

  Bikes classic350 = Bikes(
    bikeType: 'Royal Enfield Classic 350',
    brand: 'Royal Enfield',
    price: 'Rs1.93 - 2.25 Lakh*',
    image: 'https://bd.gaadicdn.com/upload/userfiles/images/5e198a49b9ac2.jpg',
  );
  insertBike(classic350);
  Bikes meteor350 = Bikes(
    bikeType: 'Royal Enfield Meteor 350',
    brand: 'Royal Enfield',
    price: 'Rs2.93 - 3.25 Lakh*',
    image: 'https://bd.gaadicdn.com/upload/userfiles/images/5e198a49b9ac2.jpg',
  );
  insertBike(meteor350);
}
