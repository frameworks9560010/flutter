import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class Model {
  final String name;
  final num price;
  final num engine;
  final num weight;
  final num speed;
  String image1 =
      'https://www.shutterstock.com/image-vector/no-image-available-picture-coming-600nw-2057829641.jpg';
  String image2 =
      'https://www.shutterstock.com/image-vector/no-image-available-picture-coming-600nw-2057829641.jpg';
  String image3 =
      'https://www.shutterstock.com/image-vector/no-image-available-picture-coming-600nw-2057829641.jpg';

  Model(
      {required this.name,
      required this.engine,
      required this.price,
      required this.weight,
      required this.speed,
      required this.image1,
      required this.image2,
      required this.image3});

  Map<String, dynamic> getmap() {
    return {
      'name': name,
      'price': price,
      'engine': engine,
      'weight': weight,
      'speed': speed,
      'image1': image1,
      'image2': image2,
      'image3': image3
    };
  }
}

Future<void> insertBikeDetails(Model obj) async {
  final localDB = await avidatabase;
  await localDB.insert(
    'Bikedetail',
    obj.getmap(),
  );
}

dynamic avidatabase;
aviDatabase() async {
  avidatabase = await openDatabase(join('getDatabasesPath()', "BikeDB9.db"),
      version: 1, onCreate: (db, version) {
    return db.execute(
      '''CREATE TABLE Bikedetail (
        name TEXT,
        image1 TEXT,
        image2 TEXT,
        image3 TEXT,
        price REAL,
        engine REAL,
        weight REAL,
        speed REAL
      )''',
    );
  });

  Model mt15details = Model(
    name: 'MT15',
    engine: 155,
    price: 2.10,
    weight: 130,
    speed: 130,
    image1: '',
    image2: '',
    image3: '',
  );
  insertBikeDetails(mt15details);
  Model classic350details = Model(
    name: 'MT15',
    engine: 155,
    price: 2.10,
    weight: 130,
    speed: 130,
    image1: '',
    image2: '',
    image3: '',
  );
  insertBikeDetails(classic350details);
  Model meteor350details = Model(
    name: 'MT15',
    engine: 155,
    price: 2.10,
    weight: 130,
    speed: 130,
    image1: '',
    image2: '',
    image3: '',
  );
  insertBikeDetails(meteor350details);
  Model mt1details = Model(
    name: 'MT15',
    engine: 155,
    price: 2.10,
    weight: 130,
    speed: 130,
    image1: '',
    image2: '',
    image3: '',
  );
  insertBikeDetails(mt1details);
  Model mt150details = Model(
    name: 'MT15',
    engine: 155,
    price: 2.10,
    weight: 130,
    speed: 130,
    image1: '',
    image2: '',
    image3: '',
  );
  insertBikeDetails(mt150details);
  Model mt17details = Model(
    name: 'MT15',
    engine: 155,
    price: 2.10,
    weight: 130,
    speed: 130,
    image1: '',
    image2: '',
    image3: '',
  );
  insertBikeDetails(mt17details);
  Model mt18details = Model(
    name: 'MT15',
    engine: 155,
    price: 2.10,
    weight: 130,
    speed: 130,
    image1: '',
    image2: '',
    image3: '',
  );
  insertBikeDetails(mt18details);
}
