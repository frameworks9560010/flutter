import 'package:bikedekho/Data/model.dart';
import 'package:bikedekho/Screens/bikedetail.dart';
import 'package:bikedekho/Screens/brand.dart';

import 'package:flutter/material.dart';

class Showbike extends StatefulWidget {
  const Showbike({super.key});

  @override
  State<Showbike> createState() => _ShowbikeState();
}

class _ShowbikeState extends State<Showbike> {
  Future<List<Model>> getBikes() async {
    final localDB = avidatabase;
    List<Map<String, dynamic>> mapEntry = await localDB.query('SHOWBIKE');
    return List.generate(
      mapEntry.length,
      (index) {
        return Model(
          name: mapEntry[index]['name'],
          engine: mapEntry[index]['engine'],
          price: mapEntry[index]['price'],
          weight: mapEntry[index]['weight'],
          speed: mapEntry[index]['speed'],
          image1: mapEntry[index]['image1'],
          image2: mapEntry[index]['image2'],
          image3: mapEntry[index]['image3'],
        );
      },
    );
  }

  List<Model> bikesDetailList = [];
  void fetchAviData(int index) async {
    _ShowbikeState brand = _ShowbikeState();
    bikesDetailList = await brand.getBikes();

    bikesDetailList1 = bikesDetailList;
    // .where((element) => element.name == bikesList1[index].brand)
    // .toList();
    print(
        '........................................bikesDetaillist........................');
    print(bikesDetailList1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 210, 208, 208),
      appBar: AppBar(
        title: const Text("Bike"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(4),
        child: ListView.builder(
            itemCount: bikesList1.length,
            itemBuilder: (context, index) {
              return Container(
                margin: const EdgeInsets.all(5),
                padding: const EdgeInsets.all(0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 200,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(bikesList1[index].image),
                              fit: BoxFit.fill)),
                      child: const Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Spacer(),
                            // Icon(Icons.favorite_border_outlined,)
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          bikesList1[index].bikeType,
                          style: const TextStyle(fontWeight: FontWeight.w500),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          bikesList1[index].price,
                          style: const TextStyle(fontWeight: FontWeight.w900),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        fetchAviData(index);
                        gindex = index;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Detail()));
                      },
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        margin: const EdgeInsets.only(left: 10, right: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: Colors.red)),
                        child: const Center(
                          child: Text(
                            "View More Detail",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    )
                  ],
                ),
              );
            }),
      ),
    );
  }
}
