import 'package:bikedekho/Data/data.dart';
import 'package:bikedekho/Data/showBikeData.dart';
import 'package:bikedekho/Screens/showbike.dart';
import 'package:flutter/material.dart';


class BikeDekho extends StatefulWidget {
  const BikeDekho({super.key});

  @override
  State<BikeDekho> createState() => _Brand();
}

List<Bikes> bikesList1 = [];

class _Brand extends State<BikeDekho> {
  Future<List<Bikes>> getBikes() async {
    final localDB = database;
    List<Map<String, dynamic>> mapEntry = await localDB.query('SHOWBIKE');
    return List.generate(mapEntry.length, (index) {
      return Bikes(
        bikeType: mapEntry[index]['bikeType'],
        brand: mapEntry[index]['brand'],
        image: mapEntry[index]['image'],
        price: mapEntry[index]['price'],
      );
    });
  }

  List<Bikes> bikesList = [];
  void fetchData(int index) async {
    _Brand brand = _Brand();
    bikesList = await brand.getBikes();

    bikesList1 = await bikesList
        .where((element) => element.brand == brandList[index].brand)
        .toList();
    print(bikesList1);
  }

  Widget card(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
        onPressed: () {
          fetchData(index);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const Showbike()),
          );
        },
        style: const ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.white),
            shape: MaterialStatePropertyAll(
              RoundedRectangleBorder(
                side: BorderSide(
                  width: 1,
                  color: Color.fromARGB(31, 7, 7, 7),
                ),
              ),
            )),
        child: Container(
          padding: const EdgeInsets.all(0),
          margin: const EdgeInsets.all(0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Image.network(
                  brandList[index].image,
                  height: 55,
                  width: 75,
                ),
              ),
              Text(
                brandList[index].brand,
                style: const TextStyle(color: Colors.black),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 235, 237, 238),
          shadowColor: Colors.black12,
          centerTitle: true,
          title: const Text(
            'BikeDekho',
            style: TextStyle(
                color: Color.fromARGB(255, 7, 7, 7),
                fontSize: 23,
                fontWeight: FontWeight.w600),
          ),
        ),
        body: GridView.builder(
          padding: const EdgeInsets.all(10),
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              childAspectRatio: 0.9, maxCrossAxisExtent: 135),
          itemCount: brandList.length,
          itemBuilder: (context, index) {
            setState(() {});
            return card(context, index);
          },
        ),
      ),
    );
  }
}
