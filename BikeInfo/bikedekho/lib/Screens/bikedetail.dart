import 'package:bikedekho/Data/model.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

dynamic gindex;

class Detail extends StatefulWidget {
  const Detail({super.key});

  @override
  State<Detail> createState() => _DetailState();
}

List<Model> bikesDetailList1 = [];

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              gradient: const LinearGradient(
                  colors: [Colors.black, Colors.white],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [0.4, 0.8]),
              borderRadius: BorderRadius.circular(10),
              boxShadow: const [
                BoxShadow(
                  blurRadius: 1,
                  offset: Offset(0, 2),
                  color: Color.fromRGBO(239, 239, 240, 1),
                )
              ]),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Specification",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        SizedBox(
                            height: 50,
                            width: 50,
                            child: Image.asset(
                              'assets/bike-motorcycle-icon.png',
                              color: Colors.white,
                            )),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          bikesDetailList1[gindex].name,
                          style: const TextStyle(color: Colors.white),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                      ],
                    ),
                    Column(children: [
                      Image.asset(
                        'assets/price1.png',
                        height: 25,
                        width: 25,
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        "${bikesDetailList1[gindex].price}",
                        style: const TextStyle(color: Colors.white),
                      ),
                    ])
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        SvgPicture.asset('assets/flash.svg'),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${bikesDetailList1[gindex].engine}",
                          style: TextStyle(color: Colors.white),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "Engine",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        SvgPicture.asset('assets/box1.svg'),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${bikesDetailList1[gindex].weight}",
                          style: const TextStyle(color: Colors.white),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "Weight",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        SvgPicture.asset('assets/timer1.svg'),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${bikesDetailList1[gindex].speed}",
                          style: const TextStyle(color: Colors.white),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "Speed",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Container(
                        height: 200,
                        width: 300,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://bd.gaadicdn.com/processedimages/yamaha/mt-15-2-0/640X309/mt-15-2-063ea25b00fa58.jpg"),
                                fit: BoxFit.fill)),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 200,
                        width: 300,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://bd.gaadicdn.com/processedimages/yamaha/mt-15-2-0/640X309/mt-15-2-063ea25b00fa58.jpg"),
                                fit: BoxFit.fill)),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 200,
                        width: 300,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://bd.gaadicdn.com/processedimages/yamaha/mt-15-2-0/640X309/mt-15-2-063ea25b00fa58.jpg"),
                                fit: BoxFit.fill)),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 50,
                      width: 200,
                      color: Colors.white,
                      child: ElevatedButton(
                        style: const ButtonStyle(
                            shape: MaterialStatePropertyAll(
                                RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)))),
                            backgroundColor:
                                MaterialStatePropertyAll(Colors.black)),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text(
                          "Ok",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
