import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:google_fonts/google_fonts.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ToDoList(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ToDoList extends StatefulWidget {
  const ToDoList({super.key});
  @override
  State createState() => _ToDoListState();
}

class _ToDoListState extends State {
  List<TextEditingController> al = [];
  int cnt = 0;
  int x = 0;

  Color cardColor() {
    x++;
    if (x == 1) {
      return const Color.fromRGBO(232, 232, 250, 1);
    } else if (x == 2) {
      return const Color.fromRGBO(232, 238, 250, 1);
    } else {
      x = 0;
      return const Color.fromRGBO(250, 249, 232, 1);
    }
  }

  Container card() {
    return Container(
      width: 400,
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: cardColor(),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          const SizedBox(
            width: 2,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  height: 52,
                  width: 52,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: Colors.white),
                  child: Image.asset('assets/image.png')),
              const SizedBox(
                height: 20,
              ),
              const Text('21/2/2024'),
            ],
          ),
          const SizedBox(
            width: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: 300,
                child: Text(
                  //al[al.length - 1],
                  'hello',
                  style: GoogleFonts.quicksand(
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontSize: 15),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const SizedBox(
                width: 300,
                child: Text(
                    'Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'),
              ),
              Row(
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.edit),
                    color: Colors.blue,
                  ),
                  IconButton(
                      onPressed: () {
                        setState(() {
                          cnt--;
                        });
                      },
                      icon: const Icon(
                        Icons.delete,
                        color: Colors.blue,
                      )),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  TextEditingController title = TextEditingController();
  //String title;
  void addData() {
    SizedBox(
      height: 400,
      width: 400,
      child: TextField(
        controller: title,
        decoration: InputDecoration(
          hintText: 'Title',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(40),
          ),
        ),
        keyboardType: TextInputType.name,
        autofocus: true,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 44, 125, 192),
        title: Text(
          'To-Do List',
          style: GoogleFonts.quicksand(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 26),
        ),
      ),
      body: ListView.builder(
        itemCount: al.length,
        itemBuilder: (context, index) {
          return card();
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          addData();
          setState(
            () {
              al.add(title);
              //cnt++;
            },
          );
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
