
y=4;
 
z = y**2;
console.log(z);

let num1 = 1;
let str = '1';

//strict equality
console.log(num1===str);

//loose equality
console.log(num1==str);

//Ternary operator

let age = 18;

let stat = (age>=18)?'vote':'Not eligible';

console.log(stat);

