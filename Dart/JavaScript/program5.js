class Demo{
	static x = 10;
	//let y =20;       //error
	
	Demo(){
		console.log("in constructor");
	}
	
	fun(){
		console.log("in non static function");
		console.log(Demo.x);
	}

	static gun(){
		console.log("in static function");
		console.log(Demo.x);
	}
}

let demo = new Demo();
demo.fun();
//Demo.fun();		//can't access non static function through class
Demo.gun();
