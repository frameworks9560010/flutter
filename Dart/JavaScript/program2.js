//Data Types
//String 
//Number :-1,2,1.2,1.32234555
//Boolean
//Undefined :- let,var
//Null

String str = 'pratik';
Number num = 10.5;
boolean bool = true;

let a = 5;
let name = 'shashi';
let  bool = true;

console.log(a);
console.log(name);
console.log(bool);

a='shashi';
console.log(a);

{
	let b =10;	//let data type can't access outside scope
	var c =10;	//var data type can access outside scope
	console.log(b);
	console.log(c);
}
//console.log(b);
console.log(c);



