

function fun(){
	console.log(arguments);

	for(let value of arguments){	//for value
		console.log(value);
	}

	for(let key in arguments){	//for key
		console.log(key);
	}
}

function gun(...arg){
	console.log(arg);
}

fun(1,2,3,4,5);		//for map

gun(1,2,3,4,5);		//for array
