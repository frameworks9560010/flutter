//object creation

let obj = {
	x:10,
	y:20,
	
	z:function(){
		console.log("Y : " + obj.y);
	}
}

/*a:function(){  		//invalid syntax,it allowed only in object
	console.log("in a");
}*/

console.log(obj.x);
console.log(obj.z);
//console.log(a);
obj.z();

