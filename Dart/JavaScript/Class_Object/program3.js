//function
//1)factory function
//2)constructor


let obj =  {
	x:1, //property
	y:2,
	fun:function(){
		console.log("hello in object run");
	},

	run(){
		console.log("in fun");
	}
}

console.log(obj.x);
obj.fun();
obj.run();



//factory function

function createobj(a,b){
	return obj1={
		a:a,
		b:b,
		c:function(){
			console.log("in object2");
		},

		d(){
			console.log("in d");
		}
	}
}

obj1 = createobj(10,20);
console.log(obj1.a);
obj1.c();
obj1.d();
