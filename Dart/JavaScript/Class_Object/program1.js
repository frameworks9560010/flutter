

class Demo{
	static x = 10;

	fun(){
		console.log(Demo.x);
		console.log("instance method");
	}

	static gun(){
		console.log(Demo.x);
		console.log("Static method");
	}

	
}

let obj = new Demo();
obj.Demo();
obj.fun();
Demo.gun();
