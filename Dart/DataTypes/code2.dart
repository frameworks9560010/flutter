
//data types

void main(){
	int x = 10;
	num y = 20;
	double z = 20.5;
	String a = "c2w";
	dynamic b = 25;

	print(x);
	print(y);
	print(z);
	print(a);
	print(b);
	
	//we cant change int value to another data type
	//x = 10.5;
	
	//num pahilyanda jo data type dila toch nantar dyava lagto
	//y = 20.5;
	

	//we can change dynamic data type any were
	b = "Pratik";
	print(b);
}
