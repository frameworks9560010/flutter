

import 'dart:io';

class Demo{
	int? x;
	static int? y;

	void getData(){
		print("Enter x");
		x=int.parse(stdin.readLineSync()!);
		
		print("Enter y");
		y=int.parse(stdin.readLineSync()!);
	}

	void printData(){
		Demo obj = new Demo();
		obj.getData();
		print("X = $x");
		print("Y = $y");
	}

}

void main(){
	Demo obj = new Demo();
	obj.printData();
}
