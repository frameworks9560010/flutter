//static variable

class Demo{
	int x=10;
	static int y=20;

	void fun(){
		print("x : $x");
		print("y : $y");
	}
}

void main(){
	Demo obj = new Demo();
	obj.fun();
	print(obj.x);
      //print(obj.y);  in dart we can't access static varible by using object, it must access through class name
	print(Demo.y);
}
