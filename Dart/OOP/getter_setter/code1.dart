
class Demo{
	//private element
	int? _x;
	Demo(this._x);

	void disp(){
		print(_x);
	}
}

void main(){
	Demo obj = new Demo(10);
	obj.disp();

	//in dart private variables can seen in same file's different classes
	obj._x=20;
	obj.disp();
}
