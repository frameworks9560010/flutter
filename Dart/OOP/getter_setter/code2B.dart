

class Demo{
	int? _x;
	Demo(this._x);
	
	//var disp = ()=>print(_x);  => in this type lambda function not support
	void disp(){
		print(_x);
	}
}
