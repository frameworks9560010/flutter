

class Demo{
	int? _X;
	int? _Y;

	Demo(this._X,this._Y);

	int? getX(){
		return _X;
	}

	int? getY(){
		return _Y;
	}
}
