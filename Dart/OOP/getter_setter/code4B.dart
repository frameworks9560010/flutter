
class Demo{
	int? _X;
	int? _Y;

	Demo(this._X,this._Y);

	int? get getX{
		return _X;
	}

	int? get getY{
		return _Y;
	}
}
