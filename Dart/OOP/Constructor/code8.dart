//named Constructor

class Demo{
	int x =10;
	int y =20;
	Demo(){
		print("In constructor");
	}

	Demo.namedConstroctor(this.x,this.y);

	void printData(){
		print("x = $x");
		print("y = $y");
	}
}

void main(){
	Demo obj = new Demo();
	Demo obj1 = new Demo.namedConstroctor(10,20);

	obj.printData();
	obj1.printData();
}
