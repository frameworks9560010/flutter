
//Parameterized constructor

class Demo{

	int? x ;
 
	Demo(int x){
		print("In default constroctor");
		this.x = x;
	}

	void printDemo(){
		print(x);
	}
}

void main(){
	int x = 10;
	Demo obj = new Demo(x);
	obj.printDemo();
}
