
class Demo{

	int x = 20;
	int y = 10;
	Demo(this.x,{this.y = 20});

	void printData(){
		print("x = $x");
		print("y = $y");
	}
}

void main(){
	Demo obj = new Demo(100);
	Demo obj1 = new Demo(200,y:300); //in this type parameter name is compolsory
	obj.printData();
	obj1.printData();
}
