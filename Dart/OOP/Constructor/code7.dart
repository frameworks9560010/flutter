
class Demo{

	int? x = 20;
	int? y = 10;
	Demo({this.x,this.y = 20});

	void printData(){
		print("x = $x");
		print("y = $y");
	}
}

void main(){
	//in this way giving parameter name is compolsory
	Demo obj = new Demo(x : 100);
	Demo obj1 = new Demo(x : 200, y : 300);
	obj.printData();
	obj1.printData();
}
