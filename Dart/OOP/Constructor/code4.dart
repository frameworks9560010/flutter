//way 4


class Demo{
	int x = 1;
	int y = 2;
	Demo(this.x,this.y);

	//var printDemo = () => print(x);

	void printDemo(){
		print("x = $x");
	}
}

void main(){
	Demo obj = new Demo(10,20);
	Demo obj1 = new Demo(30,40);
	Demo obj2 = new Demo(50,60);

	obj.printDemo();
	obj1.printDemo();
	obj2.printDemo();
}
