

class Parent{
	int x=10;
	Parent(){
		print("In parent constroctor");		
	}
	void parentMethod(){
		print(this.x);
		print(this);
		print(x);
	}
}

class Child extends Parent{
	int x=20;

	Child(){
		print("In Child constroctor");
	}

	void childMethod(){
		print(this.x);
		print(this);
		print(x);
	}

}

void main(){
	Child obj=new Child();
	Parent obj1=new Parent();
	obj.parentMethod();
	obj.childMethod();
	obj1.parentMethod();
}


