
void fun(String team,{String? name="Virat"}){
	print(team);
	print(name);
}

void main(){
	//fun("India",Rohit)  //Error
	//fun(name:Rohit) //Error
	fun("India");
	fun("India",name: "Rohit");
}
