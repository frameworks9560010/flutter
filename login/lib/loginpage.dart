import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State createState() => _LoginState();
}

class _LoginState extends State {
//   Future<List<LoginDataBase>> getData() async {
//   final localDB = await logindatabase;
//   List<Map<String, dynamic>> mapEntry = await localDB.query("loginTable");
  // return List.generate(mapEntry.length, (i) {
  //   setState(() {});
  //   // return LoginDataBase(
  //   //   email: email.text,
  //   //   name: name.text,
  //   //   password: password.text,
  //   //   userName: userName.text,
  //   // );
  // }
  //);
  //}
  TextEditingController userName = TextEditingController();
  TextEditingController passWord = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color.fromARGB(255, 1, 58, 105),
              Color.fromARGB(255, 7, 82, 143),
              Color.fromARGB(255, 36, 120, 189),
              Color.fromARGB(255, 80, 158, 221),
            ],
          ),
        ),
        child: Column(
          children: [
            //userName
            Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15, top: 20),
              child: TextField(
                controller: userName,
                decoration: const InputDecoration(
                  hintText: 'userName',
                  border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromARGB(255, 33, 243, 86)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  icon: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
              ),
            ),

            //Password
            Padding(
              padding: const EdgeInsets.only(left: 0.0, right: 15, top: 20),
              child: TextField(
                obscureText: true,
                controller: passWord,
                decoration: InputDecoration(
                  hintText: 'Password',
                  border: const OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromARGB(255, 33, 243, 86)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  icon: IconButton(
                    onPressed: () {
                      setState(() {});
                    },
                    icon: const Icon(Icons.remove_red_eye),
                    color: Colors.white,
                  ),
                ),
              ),
            ),

            //Login
            Padding(
              padding: const EdgeInsets.only(top: 70.0, left: 100, right: 100),
              child: ElevatedButton(
                onPressed: () async {
                  setState(() {});
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const LoginPage(),
                    ),
                  );
                },
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.blue)),
                child: const Center(
                  child: Text(
                    'Login',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
