import 'package:flutter/material.dart';
import 'package:login/loginData.dart';
import 'package:login/loginpage.dart';
//import 'package:login/startpage.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State createState() => _SignUpState();
}

class _SignUpState extends State<SignUpPage> {
  TextEditingController userName = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController email = TextEditingController();

  Future<List> getUsernames() async {
    final localDB = await logindatabase;
    List<Map<String, dynamic>> mapEntry = await localDB.query("loginTable");

    List usernames = mapEntry.map((entry) => entry['userName']).toList();
    return usernames;
  }

  List usernames = [];
  void someFunction() async {
    usernames = await getUsernames();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color.fromARGB(255, 1, 58, 105),
              Color.fromARGB(255, 7, 82, 143),
              Color.fromARGB(255, 36, 120, 189),
              Color.fromARGB(255, 80, 158, 221),
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Padding(
              padding: EdgeInsets.all(4.0),
              child: Text(
                'REGISTER',
                style: TextStyle(
                    color: Color.fromARGB(255, 250, 251, 252),
                    fontSize: 33,
                    fontWeight: FontWeight.w500),
              ),
            ),
            const Text(
              'create your account',
              style: TextStyle(
                  color: Color.fromARGB(255, 250, 251, 252),
                  fontSize: 15,
                  fontWeight: FontWeight.w400),
            ),
            input(name, 'Name', Icons.person, usernames),

            // input(userName, 'username', Icons.person, usernames),
            Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15, top: 20),
              child: TextField(
                controller: userName,
                decoration: const InputDecoration(
                  hintText: 'userName',
                  border: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromARGB(255, 33, 243, 86)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  icon: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            
            input(email, 'email', Icons.email, usernames),

            //password
            Padding(
              padding: const EdgeInsets.only(left: 0.0, right: 15, top: 20),
              child: TextField(
                obscureText: true,
                controller: password,
                decoration: InputDecoration(
                  hintText: 'Password',
                  border: const OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromARGB(255, 33, 243, 86)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  icon: IconButton(
                    onPressed: () {
                      setState(() {});
                    },
                    icon: const Icon(Icons.remove_red_eye),
                    color: Colors.white,
                  ),
                ),
              ),
            ),

            //Register Button
            Padding(
              padding: const EdgeInsets.only(top: 70.0, left: 100, right: 100),
              child: ElevatedButton(
                onPressed: () async {
                  if (usernames.contains(userName)) {
                    userName.clear();
                    const AlertDialog(
                      content: Text('This userName allready present'),
                    );
                    setState(() {});
                  }
                  LoginDataBase obj = LoginDataBase(
                    email: email.text,
                    name: name.text,
                    password: password.text,
                    userName: userName.text,
                  );
                  await insertData(obj);

                  setState(() {
                    someFunction();
                  });
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const LoginPage(),
                    ),
                  );
                },
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.blue)),
                child: const Center(
                  child: Text(
                    'Register',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget input(TextEditingController controller, String hintText,
    IconData iconData, List al) {
  return Padding(
    padding: const EdgeInsets.only(left: 15.0, right: 15, top: 20),
    child: TextField(
      controller: controller,
      decoration: InputDecoration(
        hintText: hintText,
        border: const OutlineInputBorder(
          borderSide: BorderSide(color: Color.fromARGB(255, 33, 243, 86)),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        icon: Icon(
          iconData,
          color: Colors.white,
        ),
      ),
    ),
  );
}
