import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class LoginDataBase {
  String name;
  String userName;
  String email;
  String password;
  LoginDataBase({
    required this.email,
    required this.name,
    required this.password,
    required this.userName,
  });

  Map<String, dynamic> bidingMap() {
    return {
      'Name': name,
      'userName': userName,
      'passWord': password,
      'email': email,
    };
  }

  @override
  String toString() {
    return '''{
     'Name': $name,
      'userName': $userName,
      'passWord': $password,
      'email': $email,
      }''';
  }
}

Future<void> insertData(LoginDataBase card) async {
  final localDB = await logindatabase;
  await localDB.insert(
    'loginTable',
    card.bidingMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

dynamic logindatabase;

dynamic loginDatabaseFunction() async {
  logindatabase = await openDatabase(
    join(await getDatabasesPath(), "LoginData104.db"),
    version: 1,
    onCreate: (db, version) {
      return db.execute('''CREATE TABLE loginTable(
        name TEXT,
        userName PRIMARY KEY,
        email TEXT,
        password TEXT)''');
    },
  );
}
