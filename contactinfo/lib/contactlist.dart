import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:call_log/call_log.dart';
import 'package:mobile_number/mobile_number.dart';

class ContactList extends StatefulWidget {
  const ContactList({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ContactListState();
  }
}

class _ContactListState extends State {
  List<Contact> contactlist = [];
  //List calllog = [];
  void getpermission() async {
    if (await Permission.contacts.isGranted) {
      contactlist = await FlutterContacts.getContacts();
      // Iterable<CallLogEntry> entries = await CallLog.get();
      String? calllog = await MobileNumber.mobileNumber;
      //print(entries);
      print(contactlist);
      print(calllog);
    } else {
      await Permission.contacts.request();
      await Permission.phone.request();
    }
  }

  @override
  void initState() {
    super.initState();
    getpermission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Contacts',
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
        ),
      ),
      body: ListView.separated(
        itemCount: contactlist.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              height: 100,
              width: MediaQuery.of(context).size.width,
              decoration: const BoxDecoration(
                  color: Colors.white,
                  border: Border(
                    top: BorderSide(color: Colors.black),
                    bottom: BorderSide(color: Colors.black),
                    left: BorderSide(color: Colors.black),
                    right: BorderSide(color: Colors.black),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromARGB(255, 208, 216, 214),
                      blurRadius: 15,
                      spreadRadius: 2,
                      offset: Offset(4, 4),
                    )
                  ]),
              child: Column(
                children: [
                  Text(contactlist[index].displayName),
                  Text('${contactlist[index].phones}'),
                ],
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox(
            height: 20,
          );
        },
      ),
    );
  }
}
